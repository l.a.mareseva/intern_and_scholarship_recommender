"""
    источники:
    https://vk.com/itmoru
    https://vk.com/itmo_exchange
    https://vk.com/careers_service_itmo
    https://vk.com/scicomm
"""
import requests
import csv
import numpy as np
import pandas as pd
import time
from datetime import datetime
import codecs
import matplotlib.pyplot as plt


class VkParser(object):

    def __init__(self):
        self.cur_link = ''
        self.key_words = ['стипендия', "стажировка", 'вакансия', 'грант']
        self.size_posts = 2000
        self.vk_sources = ['itmoru_data.csv', 'itmo_exchange_data.csv', 'scicomm_data.csv',
                           'itmomagistry_data.csv', 'careers_service_itmo_data.csv']
        self.token = 'd6e6b9e0d6e6b9e0d6e6b9e017d689dffcdd6e6d6e6b9e088d4b968d0f8ac222f404955'
        self.month_count = np.zeros((len(self.vk_sources) + 2, 12))
        self.month_count_extended = np.zeros((len(self.vk_sources)+1, len(self.key_words), 12))
        self.set_links = set()

    def start(self, new_data):
        if new_data:
            for source in self.vk_sources:
                filtered_data = self.vk_filter(self.get_posts(source.replace('_data.csv', "")))
                self.save_data(source, filtered_data)
        self.month_statistics()
        return self.vk_sources

    def get_posts(self, domain):
        verson = 5.103
        count = 100
        offset = 0
        data = []
        while offset < self.size_posts:
            response = requests.get('https://api.vk.com/method/wall.get',
                                    params={
                                        'access_token': self.token,
                                        'v': verson,
                                        'domain': domain,
                                        'count': count,
                                        'offset': offset
                                    })
            data.extend(response.json()['response']['items'])
            offset += 100
            time.sleep(1)
        return data

    def vk_filter(self, data):
        filtered_data = []
        for item in data:
            if int(datetime.utcfromtimestamp(int(item['date'])).strftime('%Y-%m-%d %H:%M:%S')[:4]) > 2018:
                if any(word in item['text'] for word in self.key_words):
                    try:
                        if any(item['attachments'][i]['type'] == 'link' for i in range(len(item['attachments']))):
                            filtered_data.append(item)
                    except KeyError:
                        pass
        return filtered_data

    def link_check(self, post):
        for i in range(len(post['attachments'])):
            if post['attachments'][i]['type'] == 'link':
                link = post['attachments'][i]['link']['url']
                if link not in self.set_links:
                    self.set_links.add(link)
                    self.cur_link = link
                    return True
                else:
                    return False

    def save_data(self, name, data):
        with codecs.open('csv storage/'+name, 'w', encoding='cp1251', errors='replace') as file:
            writer = csv.writer(file, delimiter=',')
            writer.writerow(('text', 'year', 'month', 'type', 'link'))
            for post in data:
                if self.link_check(post):
                    type_ = ''
                    for word in self.key_words:
                        if word in post['text']:
                            type_ = word
                    t = datetime.utcfromtimestamp(int(post['date'])).strftime('%Y-%m-%d %H:%M:%S')
                    writer.writerow((' '.join(post['text'].split()).replace(',', ';'), t[:4], t[5:7],
                                     type_, self.cur_link))

    def month_statistics(self):
        index = 1
        self.month_count[0] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        for sours in self.vk_sources:
            data = pd.read_csv('csv storage/'+sours, encoding='cp1251', sep=',', low_memory=False)
            # data.groupby('month')
            for i in range(1, len(data.index)):
                if data.at[i, 'month'] != 'None':
                    self.month_count[index][int(data.at[i, 'month']) - 1] += 1
            for i in range(0, len(data.index)):
                for tag_id in range(len(self.key_words)):
                    if self.key_words[tag_id] in data.at[i, 'text']:
                        self.month_count_extended[index-1][tag_id][int(data.at[i, 'month']) - 1] += 1
            index += 1
        self.month_count[index] = [x + y + z + w for x, y, z, w in
                              zip(self.month_count[1], self.month_count[2], self.month_count[3], self.month_count[4])]

    def make_grafic(self, with_bars=True):
        index = 1
        fig2, ax2 = plt.subplots(1, 1)
        for sours in self.vk_sources:
            fig, ax = plt.subplots(1, 1)
            ax.plot(self.month_count[0], self.month_count[index])
            ax2.plot(self.month_count[0], self.month_count[index], label=sours.replace('_data.csv', ''))
            plt.title('Statistics for ' + sours.replace('_data.csv', ''), fontsize=18)
            plt.savefig('plots/' + 'Statistics for ' + sours.replace('_data.csv', '') + '.png', format='png',
                        dpi=100)
            index += 1

        ax2.plot(self.month_count[0], self.month_count[index], label='all')
        plt.title('Statistics for full vk', fontsize=18)
        ax2.legend(loc='best')
        plt.savefig('plots/' + 'Statistics for full vk' + '.png', format='png', dpi=100)
        plt.show()
        if with_bars:
            # стипендия, стажировка, обмен, конкурс, вакансия, грант
            month = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
            j = np.zeros((1, 12))
            vk_s = len(self.vk_sources)
            for i in range(vk_s+1):
                fig, ax = plt.subplots(1, 1)
                ax.bar(np.array(month) - 0.3, self.month_count_extended[i][0], width=0.1, label='стипендия')
                ax.bar(np.array(month) - 0.2, self.month_count_extended[i][1], width=0.1, label='стажировка')
                ax.bar(np.array(month) - 0.1, self.month_count_extended[i][2], width=0.1, label='обмен')
                ax.bar(np.array(month), self.month_count_extended[i][3], width=0.1, label='конкурс')
                ax.bar(np.array(month) + 0.1, self.month_count_extended[i][4], width=0.1, label='вакансия')
                ax.bar(np.array(month) + 0.2, self.month_count_extended[i][5], width=0.1, label='грант')
                if i < vk_s:
                    for j in range(len(self.key_words)):
                        self.month_count_extended[vk_s][j] = \
                            [x + y for x, y in zip(self.month_count_extended[vk_s][j], self.month_count_extended[i][j])]
                    name = self.vk_sources[i]
                else:
                    name = 'all'
                plt.title('Bars for ' + name.replace('_data.csv', ''), fontsize=18)
                ax.legend(loc='best')
                plt.savefig('plots/' + 'Bars for ' + name.replace('_data.csv', '') + '.png', format='png', dpi=100)
            plt.show()
