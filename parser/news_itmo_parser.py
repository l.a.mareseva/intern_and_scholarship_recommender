from bs4 import BeautifulSoup as bs
import requests
import csv
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import codecs


class News_itmo_parser(object):

    def __init__(self):
        self.url_list = [
        'https://news.itmo.ru/ru/search/?page=1&search=%D1%81%D1%82%D0%B8%D0%BF%D0%B5%D0%BD%D0%B4%D0%B8%D0%B8&section=news',  # стипендия
        'https://news.itmo.ru/ru/search/?page=1&search=%D1%81%D1%82%D0%B0%D0%B6%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0&section=news',  # стажировка
        'https://news.itmo.ru/ru/search/?page=1&search=%D0%B3%D1%80%D0%B0%D0%BD%D1%82&section=news'  # грант
        ]
        self.names = ['news_itmo_scholarship_data.csv', 'news_itmo_internship_data.csv', 'news_itmo_grant_data.csv']
        self.all_data = []
        self.month_count = np.zeros((len(self.names) + 2, 12))

    def start(self, new_data=True):
        if new_data:
            for base_url, name in zip(self.url_list, self.names):
                data = self.get_data(base_url, name)
                self.save_data(data, name)
                self.all_data.extend(data)
            self.save_data(self.all_data)
        #sourses = self.names + ['news_itmo_data.csv']
        self.month_statistics()
        return self.names

    def get_data(self, current_url, name):
        data = []
        type_d = name.replace('news_itmo_', "").replace('_data.csv', "")
        counter = 1
        session = requests.Session()
        request = session.get(current_url)
        if request.status_code != 200:
            print('Error')
        else:
            soup = bs(request.content, 'html.parser')
            # число страниц
            for div in soup.find_all('div', attrs={'class': 'col-2 flex-center'}):
                counter = div.find_all('li')[len(div.find_all('li'))-1].text
            for i in range(int(counter)):
                url = current_url.replace('page=1', 'page='+str(i+1))
                request = session.get(url)
                if request.status_code != 200:
                    print('Error')
                else:
                    soup = bs(request.content, 'html.parser')
                    for div in soup.find_all('div', attrs={'class': 'weeklyevents'}):
                        for ul in div.find_all('ul'):
                            for li in ul.find_all('li'):
                                header = li.find('a').text
                                try:
                                    value, time = li.find_all('p')[0].text, li.find_all('p')[1].text
                                    year, month = time[6:], time[3:5]
                                    if int(year) < 2019:
                                        return data
                                except:
                                    value, month, year = 'None', 'None', 'None'
                                data.append([header, value, month, year, type_d])
            return data

    def save_data(self, data, name='news_itmo_data.csv'):
        with codecs.open('csv storage/'+name, 'w', encoding='cp1251', errors='replace') as file:
            writer = csv.writer(file, delimiter=',')
            writer.writerow(('header', 'text', 'month', 'year', 'type'))
            for post in data:
                try:
                    writer.writerow(((' '.join(post[0].split()).replace(',', ';')),
                                     (' '.join(post[1].split()).replace(',', ';')), post[2], post[3], post[4]))
                except UnicodeEncodeError:
                    pass

    def month_statistics(self):
        index = 1
        self.month_count[0] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        for sours in self.names:
            data = pd.read_csv('csv storage/'+sours, encoding='cp1251', sep=',', low_memory=False)
            # data.groupby('month')
            for i in range(1, len(data.index)):
                if data.at[i, 'month'] != 'None':
                    self.month_count[index][int(data.at[i, 'month']) - 1] += 1
            index += 1
        self.month_count[index] = [x + y + z + w for x, y, z, w in
                              zip(self.month_count[1], self.month_count[2], self.month_count[3], self.month_count[4])]

    def make_grafic(self, with_bars=True):
        index = 1
        fig2, ax2 = plt.subplots(1, 1)
        for sours in self.names:
            fig, ax = plt.subplots(1, 1)
            ax.plot(self.month_count[0], self.month_count[index])
            ax2.plot(self.month_count[0], self.month_count[index], label=sours.replace('_data.csv', ''))
            # fig.set_size_inches(22, 10)
            plt.title('Statistics for ' + sours.replace('_data.csv', ''), fontsize=18)
            plt.savefig('plots/' + 'Statistics for ' + sours.replace('_data.csv', '') + '.png', format='png',
                        dpi=100)
            index += 1
        ax2.plot(self.month_count[0], self.month_count[index], label='all')
        plt.title('Statistics for full news itmo', fontsize=18)
        ax2.legend(loc='best')
        plt.savefig('plots/' + 'Statistics for full news itmo' + '.png', format='png', dpi=100)
        plt.show()

        if with_bars:
            fig, ax = plt.subplots(1, 1)
            ax.bar(np.array(self.month_count[0]) - 0.25, self.month_count[1], width=0.2, label='scholarship')
            ax.bar(np.array(self.month_count[0]) - 0.050, self.month_count[2], width=0.2, label='internship')
            ax.bar(np.array(self.month_count[0]) + 0.15, self.month_count[3], width=0.2, label='grant')
            plt.title('Bars for full news itmo', fontsize=18)
            ax.legend()
            plt.savefig('plots/' + 'News itmo bar' + '.png', format='png', dpi=100)
            plt.show()
