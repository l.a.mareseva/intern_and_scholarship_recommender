from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
import csv
import codecs
from nltk.stem.snowball import RussianStemmer
from nltk.tokenize import RegexpTokenizer
from stop_words import get_stop_words
import re
import gensim
from gensim import models
import gensim.corpora as corpora
from gensim.utils import simple_preprocess
from gensim.models import CoherenceModel
import spacy
import pyLDAvis
import pyLDAvis.gensim  # don't skip this
import matplotlib.pyplot as plt
#%matplotlib inline
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.ERROR)
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)
import gensim.downloader as api
import artm
from spacy.lang.ru import Russian
from IPython.core.getipython import get_ipython
import TfIdf_analyser
from sklearn.externals import joblib
from gensim.test.utils import datapath


class Post(object):
    def __init__(self, text='', processed_text='', train_class='', p_class='', topic='', main_words=[], theme='', train_theme=''):
        self.text = text
        self.processed_text = processed_text  # после обработки
        self.p_class = p_class
        self.topic = topic
        self.train_class = train_class
        self.main_words = main_words
        self.theme = theme
        self.train_theme = train_theme


class Classifier(object):
    def __init__(self, sources):
        self.sources = sources
        self.train_data = {}
        self.texts = {}
        self.stemmer = RussianStemmer(False)
        self.reg_tok = RegexpTokenizer(r'\w+')
        self.stopwords = get_stop_words('russian')
        #self.model = api.load("word2vec-ruscorpora-300")
        self.nlp = Russian()
        self.train_posts = []
        self.work_posts = []
        self.topic_dict = {}
        self.test_posts = []

    def pretreatment(self, text):
        data = [re.sub(r'[^\w\s]', '', sent) for sent in text.split()]
        data_words = self.remove_stopwords(list(self.sent_to_words(data)))
        # Do lemmatization
        data_lemmatized = self.lemmatization(data_words)
        result = ' '.join([' '.join(word) for word in data_lemmatized])
        return result

    def get_data(self):
        with codecs.open('csv storage/train_texts.csv', 'r', encoding='cp1251', errors='replace') as read_file:
            reader = csv.reader(read_file, delimiter=',')
            for row in reader:
                self.train_posts.append(Post(text=row[0], processed_text=self.pretreatment(row[0]),
                                             train_class=str(row[1]), train_theme=str(row[2])))

        with codecs.open('csv storage/testing_texts.csv', 'r', encoding='cp1251', errors='replace') as read_file:
            reader = csv.reader(read_file, delimiter=',')
            for row in reader:
                self.test_posts.append(Post(text=row[0], processed_text=self.pretreatment(row[0]),
                                             train_class=str(row[1]), train_theme=str(row[2])))

        for source in self.sources:
            if 'csv storage' not in source:
                source = 'csv storage/' + source
            with codecs.open(source, 'r', encoding='cp1251', errors='replace') as read_file:
                reader = csv.reader(read_file, delimiter=',')
                next(reader)
                for row in reader:
                    self.work_posts.append(Post(text=row[0], processed_text=self.pretreatment(row[0])))

    def get_classes(self, training=False, load_model=False):
        print('getting classes')
        if training:
            working_with = self.test_posts
        else:
            working_with = self.work_posts
        if load_model:
            text_clf = joblib.load('classes_predict.pkl')
        else:
            text_clf = Pipeline([
                ('tfidf', TfidfVectorizer(stop_words=self.stopwords)),
                ('clf', RandomForestClassifier())
            ])

            text_clf.fit([post.processed_text for post in self.train_posts],
                         [post.train_class for post in self.train_posts])
            joblib.dump(text_clf, 'classes_predict.pkl')
        for post in working_with:
            post.p_class = text_clf.predict([post.processed_text])

        if training:
            counter = 0
            for post in self.test_posts:
                if post.p_class == post.train_class:
                    counter += 1
            print('Accuracy for classes: ', counter / len(self.test_posts))

    def save(self, path='csv storage/classified_posts.csv'):
        with codecs.open(path, 'w', encoding='cp1251', errors='replace') as file:
            writer = csv.writer(file, delimiter=',')
            writer.writerow(('text', 'processed_text', 'class', 'topic', 'train class', 'main_words', 'theme', 'train theme'))
            for post in self.work_posts:

                writer.writerow((post.text, post.processed_text, post.p_class,
                                 post.topic, post.train_class, ' '.join(post.main_words), post.theme, post.train_theme))
        print('Results saved to ', path)

    def sent_to_words(self, sentences):
        for sentence in sentences:
            yield (gensim.utils.simple_preprocess(str(sentence), deacc=True))  # deacc=True removes punctuations

    def remove_stopwords(self, texts):
        return [[word for word in simple_preprocess(str(doc)) if word not in self.stopwords] for doc in texts]

    def lemmatization(self, texts):
        texts_out = []
        for sent in texts:
            doc = self.nlp(" ".join(sent))
            texts_out.append([token.lemma_ for token in doc])
        return texts_out

    def get_topics(self, testing=False, load_model=False, without_main_words=True):
        print('getting topics')
        if testing:
            working_with = self.test_posts
        else:
            working_with = self.work_posts
        texts = [post.processed_text.split() for post in working_with]
        id2word = corpora.Dictionary(texts)
        # Term Document Frequency
        corpus = [id2word.doc2bow(text) for text in texts]
        if not load_model:
            # Build LDA model
            lda_model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                                                        id2word=id2word,
                                                        num_topics=30,
                                                        random_state=100,
                                                        update_every=1,
                                                        chunksize=100,
                                                        passes=10,
                                                        alpha='auto',
                                                        per_word_topics=True)
            lda_model.save(datapath("topics_model"))
            '''
            # tfudf atempt
            dictionary = gensim.corpora.Dictionary(texts)
            bow_corpus = [dictionary.doc2bow(doc) for doc in texts]
            tfidf = models.TfidfModel(bow_corpus)
            corpus_tfidf = tfidf[bow_corpus]
            lda_model_tfidf = gensim.models.LdaMulticore(corpus_tfidf, num_topics=10, id2word=dictionary, passes=2)'''
        else:
            lda_model = gensim.models.ldamodel.LdaModel.load(datapath("topics_model"))
        for idx, topic in lda_model.show_topics(num_topics=30, formatted=False, num_words=30):
            # print('Topic: {} \nWords: {}'.format(idx, '|'.join([w[0] for w in topic])))
            self.topic_dict.update({idx: [w[0] for w in topic]})

        if without_main_words:
            for i in range(len(working_with)):
                m = [0, 0]
                for topic in lda_model.get_document_topics(corpus[i]):
                    if topic[1] > m[0]:
                        m = [topic[1], topic[0]]
                working_with[i].main_words = self.topic_dict[topic[0]]

        self.classes_of_topic(testing)

    def classes_of_topic(self, testing=False):
        if testing:
            working_with = self.test_posts
        else:
            working_with = self.work_posts
        text_clf = Pipeline([
            ('tfidf', TfidfVectorizer(stop_words=self.stopwords)),
            ('clf', RandomForestClassifier())
        ])
        training = ['IT языки и технологии microsoft net core программирование программное обеспечение инженер developer'
                    'python ручное тестирование devops c/c++ opencv javascript разработка компьютер computer science java',
                    'UI/UX дизайнер реклама интерфейс макет графика визуализация',
                    'Экономика  финансы  реклама маркетолог бюджет бизнес Логистика продажа',
                    'химия генетика био рецепты соединения Bioinformatics биомолекулярнои растительный',
                    'оптика фотоника нанофотоника лазеры освещение Optometry низкотемпературная',
                    'с общим каждому член команда, глобальная программа поддержка талантливые информирование различные'
                    ' направления возможность заявка',
                    'Компания - Технологический Центр Дойче Банка. Направление - программирование; алгоритмы; структуры данных; логика. Требование: - навык программирование (Java; C++ или C#)']
        labels = ['IT', 'Дизайн', 'Экономика', 'Химия', 'Оптика', 'Общее', 'IT']
        text_clf.fit(training, labels)
        for post in working_with:
            post.theme = text_clf.predict([' '.join(post.main_words)])

        if testing:
            # считаем точность предсказанных тематик
            all_counter, posts, counter = 0, 0, 0
            count_f, num_f = 0, 0
            for post in self.test_posts:
                #print(post.text, '\n', post.main_words, '\n', post.theme, post.train_theme)
                if post.theme == post.train_theme:
                    all_counter += 1
                if post.train_class != 'другое':
                    posts += 1
                    if post.theme == post.train_theme:
                        counter += 1
                if post.train_theme != 'Общее':
                    num_f += 1
                    if post.theme == post.train_theme:
                        count_f += 1

            # для всех
            print('Accuracy for all topics:', all_counter/len(self.test_posts))
            # без тем класса "другие"
            print('Accuracy for topics except "другое":', counter / posts)
            print('Accuracy for explicit topics :', count_f / num_f)


    def get_topics_with_artm(self):
        batch_vectoriser = artm.BatchVectorizer(data_path='artm srs/texts for artm.txt', data_format='vowpal_wabbit',
                                                target_folder='artm srs')
        batch_vectoriser = artm.BatchVectorizer(data_path='artm srs', data_format='batches')
        dictionary = artm.Dictionary()
        dictionary.gather(data_path='artm srs')
        dictionary.save(dictionary_path='artm srs/my_dictionary')
        my_dictionary = dictionary.load(dictionary_path='artm srs/my_dictionary.dict')
        model_artm = artm.ARTM(num_topics=20, topic_names=['topic'+str(i) for i in range(20)], class_ids={'text': 1})
        model_artm.scores.add(artm.PerplexityScore(name='PerplexityScore', dictionary=my_dictionary))
        model_artm.scores.add(artm.SparsityPhiScore(name='SparsityPhiScore', class_id='text'))
        model_artm.scores.add(artm.SparsityThetaScore(name='SparsityThetaScore'))
        model_artm.scores.add(artm.TopTokensScore(name='top_words', num_tokens=15, class_id='text'))
        model_artm.initialize(dictionary=my_dictionary)
        model_artm.fit_offline(batch_vectorizer=batch_vectoriser, num_collection_passes=20)
        plt.plot(model_artm.score_tracker['PerplexityScore'].value)
        plt.show()

    def tf_idf_calculations(self, testing):
        if testing:
            working_with = self.test_posts
        else:
            working_with = self.work_posts
        stop_words = get_stop_words('russian')
        stop_words.extend(['программы', 'молодых', 'поддержки', 'главный', 'победитель', 'работа', 'стажировка', 'вуз',
                           'стажировки', 'итмо', 'каждыи', 'оказание', 'цель', 'какие', 'являться', 'итмостажировка',
                           'itmonews', 'работы', 'возможно', 'практики', 'прохождение', 'успешном', 'области',
                           'компании', 'компания', 'место', 'рабочих', 'общение', 'вакансия', 'неделя', 'резюме',
                           'работать', 'сфере', 'карьеры', 'площадке', 'подготовить', 'необходимости'])
        texts_list = [post.processed_text for post in working_with]
        # для каждого текста считаем коэфициент
        tfidf = TfidfVectorizer(stop_words=stop_words)
        response = tfidf.fit_transform(texts_list)
        feature_names = tfidf.get_feature_names()
        i = 0
        for post in working_with:
            l = []
            s = set()
            for col in response.nonzero()[1]:
                if feature_names[col] not in s:
                    s.add(feature_names[col])
                    l.append((feature_names[col], response[i, col]))
            post.main_words = []
            post.main_words.extend(k[0] for k in sorted(l, reverse=True, key=lambda it: it[1])[:30])
            i += 1

    def add_words(self, testing=False):
        self.tf_idf_calculations(testing)

    def start(self):
        print('Started classificator')
        self.get_data()
        #self.get_topics_with_artm()
