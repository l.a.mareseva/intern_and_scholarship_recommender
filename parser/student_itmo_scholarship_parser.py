from bs4 import BeautifulSoup as bs
import requests
import csv
import codecs


def get_data(base_url, n, data):
    local_dict = {}
    session = requests.Session()
    request = session.get(base_url)
    if request.status_code != 200:
        print('Error')
    else:
        soup = bs(request.content, 'html.parser')
        # li = soup.find('div', attrs={'class': 'weeklyevents'}).find('ul').findChildren()
        for div in soup.find_all('div', attrs={'class': 'accordion__item'}):
            title = div.find('div', attrs={'class': 'accordion__item-heading'}).find('p').text
            title = ' '.join(title.split())
            text = ''
            ul = div.find('div', attrs={'class': 'accordion__body'}).find('ul')
            try:
                for li in ul.find_all('li'):
                    text += li.text
            except AttributeError:
                pass
            local_dict.update({title: ' '.join(text.split())})
        data.append([n, local_dict])
        return data


def get_links():
    base_url = 'https://student.itmo.ru/scholarship/'
    # вытягиваем ссылки на страницы с подробным описанием
    links, names = [], []
    session = requests.Session()
    request = session.get(base_url)
    if request.status_code != 200:
        print('Error')
    else:
        soup = bs(request.content, 'html.parser')
        for div in soup.find_all('div', attrs={'class': 'accordion__body'}):
            for ul in div.find_all('ul'):
                for li in ul.find_all('li'):
                    links.append(li.find('a').get('href'))
                    names.append(li.find('a').text)
    return links, names


def save_data(data, name='sis_data.csv'):
    with codecs.open('csv storage/'+name, 'w', encoding='cp1251', errors='replace') as file:
        writer = csv.writer(file, delimiter=',')
        writer.writerow(('name', 'criterion', 'text', 'type'))
        for post in data:
            for i in range(len([*post[1]])):
                k = [*post[1]][i]
                v = post[1][k]
                try:
                    writer.writerow((post[0], k, v.replace(',', ';'), 'scholarship'))
                except UnicodeEncodeError:
                    pass


def start_parser():

    url_list, names_list = get_links()
    data = []
    for url, name in zip(url_list, names_list):
        data = get_data(url, name, data)
        save_data(data)
    #data = pd.read_csv('sis_data.csv', index_col=False, sep=',', encoding='cp1251')
    return ['sis_data.csv']
