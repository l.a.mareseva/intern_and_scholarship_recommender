"""
    источники:
    https://news.itmo.ru/ru/
    https://student.itmo.ru/scholarship/
    https://student-agency.ru/
    https://grantist.com/obuchenie/bachelor/
    https://grantist.com/obuchenie/master/
    http://www.rsci.ru/
    + vk
"""
from vk_parser import VkParser
from news_itmo_parser import News_itmo_parser
from TfIdf_analyser import TfIdf_analyser
import student_itmo_scholarship_parser as sisp


class MyParser(object):

    def __init__(self):
        self.sources = []

    def start(self, sources, visualize=False, refresh=True):
        if 'news_itmo' in sources:
            news_itmo_parser = News_itmo_parser()
            self.sources += news_itmo_parser.start(new_data=refresh)
            if visualize:
                news_itmo_parser.make_grafic(with_bars=True)
        if 'vk' in sources:
            vk_parser = VkParser()
            self.sources += vk_parser.start(new_data=refresh)
            if visualize:
                vk_parser.make_grafic(with_bars=True)
        if 'students_itmo' in sources:
            self.sources += sisp.start_parser()
        # tfidf_analysis = TfIdf_analyser(self.sources)
        # tfidf_analysis.start()
        return self.sources
