from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from scipy.spatial.distance import cosine
import pandas as pd
import numpy as np
import codecs
import csv
import time
import json
from stop_words import get_stop_words

class TfIdf_analyser(object):

    def __init__(self, sources):
        self.unity_texts = pd.DataFrame()
        self.sources = sources
        self.possible_headers = ['name', 'criterion', 'text', 'header']
        self.great_dict = {}
        self.text_vectors = {}

    def combine_text(self):
        # объединяем все тексты в один объект для анализа частоты слов и тд
        # (к основному тексту добавляем также заголовок, если есть
        for source in self.sources:
            data = pd.read_csv('csv storage/' + source, encoding='cp1251', sep='/', low_memory=False)
            df = pd.DataFrame({'text': []})
            heads = list(set(list(data.columns.values)) & set(self.possible_headers))
            if 'name' in heads:
                df['text'] = [' '.join((str(x) for x in i)) for i in zip(data['name'], data['criterion'], data['text'])]
            elif 'header' in heads:
                df['text'] = [' '.join((str(x) for x in i)) for i in zip(data['header'], data['text'])]
            else:
                df['text'] = data['text']
            self.unity_texts = pd.concat([self.unity_texts, df], ignore_index=True)

    def tf_idf_calculations(self):
        stop_words = get_stop_words('russian')
        texts_list = list(self.unity_texts['text'])
        # для каждого текста считаем коэфициент
        tfidf = TfidfVectorizer(stop_words=stop_words)
        response = tfidf.fit_transform(texts_list)
        feature_names = tfidf.get_feature_names()
        result = {}
        #print(1)
        for i in range(len(texts_list)):
            l = []
            for col in response.nonzero()[1]:
                #print(feature_names[col], ' - ', response[i, col])
                result.update({feature_names[col]: response[i, col]})
                l.append(response[i, col])
            # сохраняем результат в словарь
            self.great_dict[texts_list[i]] = result
            self.text_vectors[texts_list[i]] = l
            print(i)
        with codecs.open("data_file.json", "w", encoding='cp1251', errors='replace') as write_file:
            json.dump(self.text_vectors, write_file)
        '''
        lsa = TruncatedSVD(n_components=response.shape[0], n_iter=100)
        lsa.fit(response)
        
        t = input()
        for i, comp in enumerate(lsa.components_):
            features_comp = zip(feature_names, comp)
            sorted_features = sorted(features_comp, key=lambda x: x[1], reverse=True)[:10]
            print('concept ', i)
            for feature in sorted_features:
                print(feature[0])
            '''
        # print(1)

    def cos_sim_calculation(self):
        names = self.text_vectors.keys()
        for name in names:
            base_vec = self.text_vectors.pop(name)
            vectors = self.text_vectors.values()
            distances = {}
            i = 1
            #print(vectors)
            for current_vec in vectors:
                key = str(name)+str(i)
                distances.update({key: cosine(base_vec, current_vec)})
                i += 1
                t = input()
            # переносим проверенный элемент в конец
            self.text_vectors.update({name:base_vec})

    def analysis(self, flag):
        print('analysis started')
        if flag:
            self.tf_idf_calculations()
        else:
            with codecs.open("data_file.json", "r", encoding='cp1251', errors='replace') as read_file:
                data = json.load(read_file)
        print(data)
        print('tf_idf done')
        self.cos_sim_calculation()



    def start(self):
        self.combine_text()
        self.unity_texts.to_csv('csv storage/' + "all texts.csv", encoding='cp1251', sep="/")
        self.analysis(True)
